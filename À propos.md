## A propos du site
Ce site se veut principalement à  rassembler des liens et des outils pertinents à la pratique en pharmacie communautaire. Il est principalement adressé aux pharmaciens du Québec.
Il est aussi pour moi l’occasion de mettre en pratique mes aptitudes de programmation.
- - - -
## Avis de confidentialité et vie privée
#### Collecte d’information personnelles
Le site ne collecte aucune information personnelle ou qui permet de vous identifier.
Des statistiques de visite des liens vers les ressources externes sont compilés pour améliorer l’expérience utilisateur du site et la recherche.
Les renseignements que vous fournissez dans les outils de calculs peuvent être enregistré par votre navigateur web sur le poste que vous utilisez. Il ne sont pas stockés par le site
Aux fins d’analyse d’audience du site, des _cookies_ de mesure d’audience de la solution [Google Analytics](https://support.google.com/analytics/answer/6004245?hl=fr) sont enregistrés par votre navigateur pendant votre visite.
#### Publicité
Il n’y a **aucune** publicité sur le site, point. Aucun renseignement à des fins publicitaire n’est collecté.
- - - -
### À Propos de moi
#### Études
* Études en cours au microprogramme en gestion de pharmacie communautaire à l’Université Laval
* Études à temps partiel au certificat en informatique à l’Université Laval, 2015-2017
* Doctorat de 1er cycle en pharmacie, Université  Laval, 2015.
* Deux ans d’études temps plein au baccalauréat en sciences biomédicales à L’Université de Montréal
#### Champs d’intérêts
* La pharmacie (bien évidemment)
* Programmation web (principalement les framework [React](https://reactjs.org) et [Meteor](https://www.meteor.com) )
* ☕️
* 🚴‍
